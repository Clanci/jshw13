// Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
// setTimeout() - запускає блок коду після вказаного проміжку часу, тобто затримка йде перед викликом ф-ії
// setInterval() - повторює блок коду через вказаний проміжок часу, тобто затримка між кожним повторним викликом ф-ії

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// функція setTimeout() може не запуститися миттєво, оскільки вона буде чекати завершення поточного потоку виконання коду.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//  Коли таймер більше не потрібен слід пам'ятати викликати clearInterval(), щоб ефективно управляти ресурсами та уникнути непотрібного виконання коду.

let currentImageIndex = 0;
let slideshowInterval;
const images = document.querySelectorAll('.image-to-show');
const timerElement = document.getElementById('timer');

function showImage(index) {
    images.forEach((image, i) => {
        if (i === index) {
            image.style.opacity = '1';
        } else {
            image.style.opacity = '0';
        }
    });
}

function startSlideshow() {
    let countdown = 0;

    function changeImage() {
        currentImageIndex = (currentImageIndex + 1) % images.length;
        showImage(currentImageIndex);

        countdown = 3;
        updateTimer(countdown);
    }

    showImage(currentImageIndex);
    countdown = 3;
    updateTimer(countdown);

    slideshowInterval = setInterval(() => {
        countdown--;
        if (countdown === 0) {
            changeImage();
        } else {
            updateTimer(countdown);
        }
    }, 1000);
}

function stopSlideshow() {
    clearInterval(slideshowInterval);
}

function updateTimer(countdown) {
    timerElement.innerText = `Next image in ${countdown} seconds`;
}
